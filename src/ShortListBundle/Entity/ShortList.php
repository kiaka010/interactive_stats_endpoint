<?php

namespace ShortListBundle\Entity;
class ShortList {
    protected $formation;

    /** @var  String */
    protected $type;

    /** @var  ParticipantList */
    protected $participants;

    /**
     * @return mixed
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * @param mixed $formation
     * @return ShortList
     */
    public function setFormation($formation)
    {
        $this->formation = $formation;
        return $this;
    }

    /**
     * @return String
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param String $type
     * @return ShortList
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return ParticipantList
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @param ParticipantList $participants
     * @return ShortList
     */
    public function setParticipants($participants)
    {
        $this->participants = $participants;
        return $this;
    }


}