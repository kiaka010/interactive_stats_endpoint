<?php

namespace ShortListBundle\Entity;

class ParticipantList extends \ArrayObject {
    /**
     * Constructor
     * Ensures $input only contains type SDC\Sport\Base\Season\Entity\Season
     *
     * @param array  $input
     * @param int    $flags
     * @param string $iteratorClass
     */
    public function __construct(
        array $input = [],
        $flags = 0,
        $iteratorClass = "ArrayIterator"
    ) {
        if (false === $this->validateInput($input)) {
            throw new \DomainException(
                'Only elements of the type ' .
                '\StatBundle\Entity\Participant are accepted.'
            );
        }
        parent::__construct($input, $flags, $iteratorClass);
    }

    /**
     * Check each element of $input is a Season
     * @param array $input
     * @return boolean
     */
    private function validateInput(array $input)
    {
        foreach ($input as $item) {
            if (! $item instanceof Participant) {
                return false;
            }
        }
        return true;
    }

    /**
     * Adds an Participant
     * @param Participant $item
     */
    public function add(Participant $item)
    {
        $this->append($item);
    }


}