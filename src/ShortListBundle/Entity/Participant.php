<?php
namespace ShortListBundle\Entity;

class Participant {

    /** @var  int */
    protected $id;

    /** @var  String */
    protected $longName;

    /** @var  String */
    protected $shortName;

    /** @var  String */
    protected $image;
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Participant
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return String
     */
    public function getLongName()
    {
        return $this->longName;
    }

    /**
     * @param String $longName
     * @return Participant
     */
    public function setLongName($longName)
    {
        $this->longName = $longName;
        return $this;
    }

    /**
     * @return String
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param String $shortName
     * @return Participant
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
        return $this;
    }

    /**
     * @return String
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param String $image
     * @return Participant
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

}