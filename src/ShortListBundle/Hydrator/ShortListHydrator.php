<?php

namespace ShortListBundle\Hydrator;

use ShortListBundle\Entity\Participant;
use ShortListBundle\Entity\ParticipantList;
use ShortListBundle\Entity\ShortList;
use ShortListBundle\Utils\PropValueTrait;


class ShortListHydrator {
    use PropValueTrait;
    protected $useParticipantImage = false;
    public function hydrate(ShortList $list, $data)
    {
        $this->useParticipantImage = $this->getPropValue($data, 'useParticipantImage');
        $list->setType($this->getPropValue($data,'type'));
        $list->setFormation($this->getPropValue($data,'formation'));
        $list->setParticipants($this->hydrateParticipantList(new ParticipantList(), $this->getPropValue($data,'participants')));
        return $list;
    }


    /**
     * @param ParticipantList $list
     * @param $data
     * @return ParticipantList
     */
    private function hydrateParticipantList(ParticipantList $list, $data)
    {

        foreach ($data as $item) {
            $participant = new Participant();
            $participant->setId($this->getPropValue($item, 'id'));
            $participant->setLongName($this->getPropValue($item, 'longName'));
            $participant->setShortName($this->getPropValue($item, 'shortName'));

            if($this->useParticipantImage) {
                $participant->setImage($this->getPropValue($item, 'image'));
            } else {
                $participant->setImage($this->getPropValue($this->getPropValue($item,'team'), 'image'));
            }
            $list->add($participant);
        }
        return $list;

    }

}