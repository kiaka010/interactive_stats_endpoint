<?php
namespace StatBundle\Mapper;

use Monolog\Logger;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StatMapper {

    function __construct(ContainerInterface $container, Logger $logger)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    public function getById($statId)
    {
        $baseUrl = $this->container->getParameter('stats_url_base');
        $url = sprintf('%s/interactive/stats/%s?v=1&Authorization=auth',$baseUrl,  $statId);
        $this->logger->info($url);
//        dump(sprintf('http://uatapi.condatis.sky/interactive/stats/%s?v=1&Authorization=a9a00d56bb6ad32a8d59cabc2f555d41', $statId));
// Get cURL resource
        $curl = curl_init();
// Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => 'Codular Sample cURL Request'
        ));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: uksportweb'
        ));

        $resp = curl_exec($curl);

        if (!curl_errno($curl)) {
            switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
                case 200:  # OK
                    break;
                default:
                    throw new Exception('Unexpected Resp Code for: ' . $url);
            }
        }
// Close request to clear up some resources
        curl_close($curl);
        return $resp;
    }
}