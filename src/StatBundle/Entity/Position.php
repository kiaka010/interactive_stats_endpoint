<?php
namespace StatBundle\Entity;

class Position {

    /** @var  int */
    protected $id;

    /** @var  ParticipantList */
    protected $participants;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Position
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return ParticipantList
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    public function setParticipants($participants)
    {
        $this->participants = $participants;
        return $this;
    }

    public function getParticipantCount()
    {
        return count($this->getParticipants());
    }


}