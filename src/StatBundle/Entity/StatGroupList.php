<?php
namespace StatBundle\Entity;

class StatGroupList {
    /** @var  int */
    protected $id;

    /** @var  int */
    protected $count;

    /** @var  String */
    protected $defaultFormation;
    /**
     * @var PositionGroupList
     */
    protected $positionGroups;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return StatGroupList
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        $this->count = 0;
        $pos = $this->getPositions();
        foreach ($pos[0]->getParticipants() as $participant)
        {
            $this->count += $participant->getScore();
        }
        return $this->count;
    }

    /**
     * @param int $count
     * @return StatGroupList
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return String
     */
    public function getDefaultFormation()
    {
        return $this->defaultFormation;
    }

    /**
     * @param String $defaultFormation
     * @return StatGroupList
     */
    public function setDefaultFormation($defaultFormation)
    {
        $this->defaultFormation = $defaultFormation;
        return $this;
    }

    /**
     * @return PositionGroupList
     */
    public function getPositionGroups()
    {
        return $this->positionGroups;
    }

    /**
     * @param PositionGroupList $positionGroups
     * @return StatGroupList
     */
    public function setPositionGroups($positionGroups)
    {
        $this->positionGroups = $positionGroups;
        return $this;
    }



}