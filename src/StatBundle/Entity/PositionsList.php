<?php
 namespace StatBundle\Entity;

 class PositionsList extends \ArrayObject {
     /**
      * Constructor
      * Ensures $input only contains type \StatBundle\Entity\Position
      *
      * @param array  $input
      * @param int    $flags
      * @param string $iteratorClass
      */
     public function __construct(
         array $input = [],
         $flags = 0,
         $iteratorClass = "ArrayIterator"
     ) {
         if (false === $this->validateInput($input)) {
             throw new \DomainException(
                 'Only elements of the type ' .
                 '\StatBundle\Entity\Position are accepted.'
             );
         }
         parent::__construct($input, $flags, $iteratorClass);
     }

     /**
      * Check each element of $input is a Season
      * @param array $input
      * @return boolean
      */
     private function validateInput(array $input)
     {
         foreach ($input as $item) {
             if (! $item instanceof Position) {
                 return false;
             }
         }
         return true;
     }

     /**
      * Adds an Position
      * @param Position $item
      */
     public function add(Position $item)
     {
         $this->append($item);
     }


 }