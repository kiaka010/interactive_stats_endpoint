<?php
namespace StatBundle\Entity;

class PositionGroup extends Position {

    /** @var  String */
    protected $groupName;

    /** @var  String */
    protected $subGroupName;

    /** @var  String */
    protected $positionName;

    /** @var  String */
    protected $positionDisplayName;

    /** @var  ParticipantList */
    protected $participants;

    /**
     * @return String
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * @param String $groupName
     * @return PositionGroup
     */
    public function setGroupName($groupName)
    {
        $this->groupName = $groupName;
        return $this;
    }

    /**
     * @return String
     */
    public function getSubGroupName()
    {
        return $this->subGroupName;
    }

    /**
     * @param String $subGroupName
     * @return PositionGroup
     */
    public function setSubGroupName($subGroupName)
    {
        $this->subGroupName = $subGroupName;
        return $this;
    }

    /**
     * @return String
     */
    public function getPositionName()
    {
        return $this->positionName;
    }

    /**
     * @param String $positionName
     * @return PositionGroup
     */
    public function setPositionName($positionName)
    {
        $this->positionName = $positionName;
        return $this;
    }

    /**
     * @return String
     */
    public function getPositionDisplayName()
    {
        return $this->positionDisplayName;
    }

    /**
     * @param String $positionDisplayName
     * @return PositionGroup
     */
    public function setPositionDisplayName($positionDisplayName)
    {
        $this->positionDisplayName = $positionDisplayName;
        return $this;
    }

}