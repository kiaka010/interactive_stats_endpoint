<?php
namespace StatBundle\Entity;

class StatList {
    /** @var  int */
    protected $id;

    /** @var  int */
    protected $count = 0;

    /** @var  String */
    protected $defaultFormation;

    /**
     * @var PositionsList
     */
    protected $positions;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return StatList
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        $this->count = 0;
        $pos = $this->getPositions();
        if (empty((array) $pos))
        {
            return 0;
        }
        foreach ($pos[0]->getParticipants() as $participant)
        {
            $this->count += $participant->getScore();
        }
        return $this->count;
    }

    /**
     * @return String
     */
    public function getDefaultFormation()
    {
        return $this->defaultFormation;
    }

    /**
     * @param String $defaultFormation
     * @return StatList
     */
    public function setDefaultFormation($defaultFormation)
    {
        $this->defaultFormation = $defaultFormation;
        return $this;
    }

    /**
     * @param int $count
     * @return StatList
     */
    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    /**
     * @return PositionsList
     */
    public function getPositions()
    {
        $this->positions->asort();
        return $this->positions;
    }

    /**
     * @param PositionsList $positions
     * @return StatList
     */
    public function setPositions($positions)
    {
        $this->positions = $positions;
        return $this;
    }

}