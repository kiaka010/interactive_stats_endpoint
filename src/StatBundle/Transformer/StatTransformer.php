<?php
namespace StatBundle\Transformer;

class StatTransformer
{
    public static function transformRaw($data)
    {
        return json_decode($data);
    }
}