<?php

namespace StatBundle\Utils;

/**
 * Trait providing object property value helpers
 */
trait PropValueTrait
{
    /**
     * Return the value of $obj->$prop if it exists, or null if not
     *
     * @param  object $obj  The object
     * @param  string $prop The property name
     * @return mixed        The property value or null if property doesn't exist
     */
    protected function getPropValue($obj, $prop)
    {
        if (!property_exists($obj, $prop)) {
            return null;
        }

        return $obj->$prop;
    }
}
