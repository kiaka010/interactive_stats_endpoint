<?php
namespace StatBundle\Hydrator;

class GolfRyderCupStatsStatGroupHydrator extends AbstractStatGroupHydrator{

    protected function getData()
    {
$json = <<<EOF
{
  "0": {
    "name": "wildcards-eu",
    "positions": {
      "1": {
        "id": "1",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "2": {
        "id": "2",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "3": {
        "id": "3",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "4": {
        "id": "4",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "5": {
        "id": "5",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "6": {
        "id": "6",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "7": {
        "id": "7",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "8": {
        "id": "8",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      }
    }
  },
  "1": {
    "name": "pairs-eu",
    "positions": {
      "1": {
        "id": "1",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "2": {
        "id": "2",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "3": {
        "id": "3",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "4": {
        "id": "4",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "5": {
        "id": "5",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "6": {
        "id": "6",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "7": {
        "id": "7",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "8": {
        "id": "8",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "9": {
        "id": "9",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "10": {
        "id": "10",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "11": {
        "id": "11",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "12": {
        "id": "12",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "13": {
        "id": "13",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "14": {
        "id": "14",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "15": {
        "id": "15",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "16": {
        "id": "16",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      }
    }
  },
  "2": {
    "name": "singles-eu",
    "positions": {
      "1": {
        "id": "1",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "2": {
        "id": "2",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "3": {
        "id": "3",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "4": {
        "id": "4",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "5": {
        "id": "5",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "6": {
        "id": "6",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "7": {
        "id": "7",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "8": {
        "id": "8",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "9": {
        "id": "9",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "10": {
        "id": "10",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "11": {
        "id": "11",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "12": {
        "id": "12",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "13": {
        "id": "13",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "14": {
        "id": "14",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "15": {
        "id": "15",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "16": {
        "id": "16",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "17": {
        "id": "15",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "18": {
        "id": "16",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "19": {
        "id": "15",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "20": {
        "id": "16",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "21": {
        "id": "15",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "22": {
        "id": "16",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "23": {
        "id": "15",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "24": {
        "id": "16",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      }
    }
  },
  "3": {
    "name": "wildcards-usa",
    "positions": {
      "1": {
        "id": "1",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "2": {
        "id": "2",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "3": {
        "id": "3",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "4": {
        "id": "4",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "5": {
        "id": "5",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "6": {
        "id": "6",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "7": {
        "id": "7",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "8": {
        "id": "8",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      }
    }
  },
  "4": {
    "name": "pairs-usa",
    "positions": {
      "1": {
        "id": "1",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "2": {
        "id": "2",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "3": {
        "id": "3",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "4": {
        "id": "4",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "5": {
        "id": "5",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "6": {
        "id": "6",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "7": {
        "id": "7",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "8": {
        "id": "8",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "9": {
        "id": "9",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "10": {
        "id": "10",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "11": {
        "id": "11",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "12": {
        "id": "12",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "13": {
        "id": "13",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "14": {
        "id": "14",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "15": {
        "id": "15",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "16": {
        "id": "16",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      }
    }
  },
  "5": {
    "name": "singles-usa",
    "positions": {
      "1": {
        "id": "1",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "2": {
        "id": "2",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "3": {
        "id": "3",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "4": {
        "id": "4",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "5": {
        "id": "5",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "6": {
        "id": "6",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "7": {
        "id": "7",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "8": {
        "id": "8",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "9": {
        "id": "9",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "10": {
        "id": "10",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "11": {
        "id": "11",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "12": {
        "id": "12",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "13": {
        "id": "13",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "14": {
        "id": "14",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "15": {
        "id": "15",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "16": {
        "id": "16",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "17": {
        "id": "15",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "18": {
        "id": "16",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "19": {
        "id": "15",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "20": {
        "id": "16",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "21": {
        "id": "15",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "22": {
        "id": "16",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      },
      "23": {
        "id": "15",
        "group": "default",
        "subGroup": "usa",
        "positionName": "usa",
        "positionDisplayName": "USA",
        "positionVariations": [
          "usa"
        ]
      },
      "24": {
        "id": "16",
        "group": "default",
        "subGroup": "europe",
        "positionName": "europe",
        "positionDisplayName": "Europe",
        "positionVariations": [
          "europe"
        ]
      }
    }
  }
}

EOF;

        return json_decode($json)->{$this->getFormation()};
    }

    private function getFormation()
    {
        switch ($this->formation)
        {
            case 'wildcards-eu':
                return 0;
            case 'pairs-eu':
                return 1;
            case 'singles-eu':
                return 2;
            case 'wildcards-usa':
                return 3;
            case 'pairs-usa':
                return 4;
            case 'singles-usa':
                return 5;
            default:
                throw new \Exception("Formation not found: $this->formation");
        }

    }
}