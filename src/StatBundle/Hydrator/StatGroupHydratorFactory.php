<?php
namespace StatBundle\Hydrator;


use Symfony\Component\Config\Definition\Exception\Exception;

class StatGroupHydratorFactory {

    protected $formation;
    function __construct($formation)
    {
        $this->formation = $formation;
    }

    /**
     * https://git.bskyb.com/sky-components/blackjack-sdc-interactive-selector/blob/master/src/components/static-data.js
     * @param $type
     * @return FootballStatsStatGroupHydrator|RugbyUnionStatsStatGroupHydrator|AbstractStatGroupHydrator
     */
    public function create($type)
    {
        dump($type);
//            dump($this->formation);die();
        switch ($type) {
            case 'football':
                return new FootballStatsStatGroupHydrator($this->formation);
            case 'rugbyunion':
                return new RugbyUnionStatsStatGroupHydrator($this->formation);
            case 'cricket':
                return new CricketStatsStatGroupHydrator($this->formation);
            case 'golfrydercup':
                return new GolfRyderCupStatsStatGroupHydrator($this->formation);
        }
        throw new Exception("Group hydrator not found");
    }
}