<?php
namespace StatBundle\Hydrator;

class CricketStatsStatGroupHydrator extends AbstractStatGroupHydrator{

    protected function getData()
    {
$json = <<<EOF
{
  "default": {
    "name": "default",
    "openPositions": "true",
    "positions": {
      "1": {
        "id": "1",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      },
      "2": {
        "id": "2",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      },
      "3": {
        "id": "3",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      },
      "4": {
        "id": "4",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      },
      "5": {
        "id": "5",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      },
      "6": {
        "id": "6",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      },
      "7": {
        "id": "7",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      },
      "8": {
        "id": "8",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      },
      "9": {
        "id": "9",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      },
      "10": {
        "id": "10",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      },
      "11": {
        "id": "11",
        "group": "default",
        "subGroup": "default",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": []
      }
    }
  }
}

EOF;

        return json_decode($json)->{$this->getFormation()};
    }

    private function getFormation()
    {
        if($this->formation == 0) {
            return 'default';
        }
        return $this->formation;
    }
}