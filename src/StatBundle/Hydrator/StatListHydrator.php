<?php

namespace StatBundle\Hydrator;

use ShortListBundle\Entity\ShortList;
use StatBundle\Entity\Participant;
use StatBundle\Entity\ParticipantList;
use StatBundle\Entity\Position;
use StatBundle\Entity\PositionsList;
use StatBundle\Entity\StatList;
use StatBundle\Utils\PropValueTrait;

class StatListHydrator {
    use PropValueTrait;
    protected $limit;
    function __construct($limit = null)
    {
        $this->limit = $limit;
    }

    public function hydrate(StatList $list, $data)
    {
        $list->setId($this->getPropValue($data, 'id'));
        $list->setPositions($this->hydratePositionsList(new PositionsList(), $data->positions));
        return $list;
    }

    private function hydratePositionsList(PositionsList $list, $data)
    {
        foreach ($data as  $item) {


            $pos = new Position();
            $pos->setId($this->getPropValue($item, 'listPosition'));
            $pos->setParticipants($this->hydrateParticipantList(new ParticipantList(), $item->participants));
            $list->add($pos);
        }
        return $list;
    }

    private function hydrateParticipantList(ParticipantList $list, $data)
    {
        usort($data, function($a, $b)
        {
            if($a->score < $b->score)
                return 1;
            return 0;
        });

        foreach ($data as $item) {

            $participant = new Participant();
            $participant->setId($this->getPropValue($item, 'id'));
            $participant->setScore($this->getPropValue($item, 'score'));
            $list->add($participant);
        }
        return $list;

    }


    public function hydrateParticipantFromShortList(StatList $list, ShortList $shortList)
    {
        foreach ($list->getPositions() as $position) {
            foreach ($position->getParticipants() as $participant) {

                $searchedValue = $participant->getId();
                $neededObject = array_values(array_filter(
                    $shortList->getParticipants()->getArrayCopy(),
                    function ($e) use (&$searchedValue) {
                        return $e->getId() == $searchedValue;
                    }
                ));
                $participant->setName($neededObject[0]->getLongName());
                $participant->setImage($neededObject[0]->getImage());

            }
        }
        return $list;
    }

    /**
     * @param StatList $list
     * @param ShortList $shortList
     * @return StatList
     */
    public function hydrateFormationFromShortList(StatList $list, ShortList $shortList)
    {
        $list->setDefaultFormation($shortList->getFormation());
        return $list;
    }

}

