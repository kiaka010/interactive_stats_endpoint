<?php
namespace StatBundle\Hydrator;

class RugbyUnionStatsStatGroupHydrator extends AbstractStatGroupHydrator{

    protected function getData()
    {
        $json = <<<EOF
{
  "32325": {
    "positions": {
      "1": {
        "id": "1",
        "label": "1",
        "group": "forwards",
        "subGroup": "frontRow",
        "positionName": "looseHeadProp",
        "positionDisplayName": "Loose Head Prop",
        "filter": [
          "Loose Head Prop",
          "Prop",
          "Front Row"
        ],
        "x": 10,
        "y": 8
      },
      "2": {
        "id": "2",
        "label": "2",
        "group": "forwards",
        "subGroup": "frontRow",
        "positionName": "hooker",
        "filter": [
          "Hooker",
          "Front Row"
        ],
        "positionDisplayName": "Hooker",
        "x": 35,
        "y": 8
      },
      "3": {
        "id": "3",
        "label": "3",
        "group": "forwards",
        "subGroup": "frontRow",
        "positionName": "tightHeadProp",
        "positionDisplayName": "Tight Head Prop",
        "filter": [
          "Tight Head Prop",
          "Prop",
          "Front Row"
        ],
        "x": 60,
        "y": 8
      },
      "4": {
        "id": "4",
        "label": "4",
        "group": "forwards",
        "subGroup": "secondRow",
        "positionName": "lock",
        "positionDisplayName": "Lock",
        "filter": [
          "Lock",
          "Second Row"
        ],
        "x": 20,
        "y": 18
      },
      "5": {
        "id": "4",
        "label": "5",
        "group": "forwards",
        "subGroup": "secondRow",
        "positionName": "lock",
        "positionDisplayName": "Lock",
        "filter": [
          "Lock",
          "Second Row"
        ],
        "x": 50,
        "y": 18
      },
      "6": {
        "id": "5",
        "label": "6",
        "group": "forwards",
        "subGroup": "backRow",
        "positionName": "blindsideFlanker",
        "positionDisplayName": "Blindside Flanker",
        "filter": [
          "Blindside Flanker",
          "Flanker",
          "Back Row"
        ],
        "x": 5,
        "y": 28
      },
      "7": {
        "id": "6",
        "label": "7",
        "group": "forwards",
        "subGroup": "backRow",
        "positionName": "opensideFlanker",
        "positionDisplayName": "Openside Flanker",
        "filter": [
          "Openside Flanker",
          "Flanker",
          "Back Row"
        ],
        "x": 65,
        "y": 28
      },
      "8": {
        "id": "7",
        "label": "8",
        "group": "forwards",
        "subGroup": "backRow",
        "positionName": "no8",
        "positionDisplayName": "Number 8",
        "filter": [
          "No 8",
          "Back Row"
        ],
        "x": 35,
        "y": 33
      },
      "9": {
        "id": "8",
        "label": "9",
        "group": "backs",
        "subGroup": "halfBacks",
        "positionName": "scrumHalf",
        "positionDisplayName": "Scrum Half",
        "filter": [
          "Scrum Half"
        ],
        "x": 51,
        "y": 43
      },
      "10": {
        "id": "9",
        "label": "10",
        "group": "backs",
        "subGroup": "halfBacks",
        "positionName": "flyHalf",
        "positionDisplayName": "Fly Half",
        "filter": [
          "Fly Half"
        ],
        "x": 62,
        "y": 54
      },
      "11": {
        "id": "11",
        "label": "12",
        "group": "backs",
        "subGroup": "centres",
        "positionName": "insideCentre",
        "positionDisplayName": "Inside Centre",
        "filter": [
          "Inside Centre",
          "Centre"
        ],
        "x": 73,
        "y": 63
      },
      "12": {
        "id": "12",
        "label": "13",
        "group": "backs",
        "subGroup": "centres",
        "positionName": "outsideCentre",
        "positionDisplayName": "Outside Centre",
        "filter": [
          "Outside Centre",
          "Centre"
        ],
        "x": 84,
        "y": 73
      },
      "13": {
        "id": "10",
        "label": "11",
        "group": "backs",
        "subGroup": "wingers",
        "positionName": "leftWing",
        "positionDisplayName": "Left Winger",
        "filter": [
          "Left Wing",
          "Winger"
        ],
        "x": 5,
        "y": 83
      },
      "14": {
        "id": "13",
        "label": "14",
        "group": "backs",
        "subGroup": "wingers",
        "positionName": "rightWing",
        "positionDisplayName": "Right Winger",
        "filter": [
          "Right Wing",
          "Winger"
        ],
        "x": 95,
        "y": 83
      },
      "15": {
        "id": "14",
        "label": "15",
        "group": "backs",
        "subGroup": "fullBacks",
        "positionName": "fullBack",
        "positionDisplayName": "Full Back",
        "filter": [
          "Full Back"
        ],
        "x": 50,
        "y": 93
      }
    }
  }
}

EOF;
        return json_decode($json)->{$this->formation};
    }
}