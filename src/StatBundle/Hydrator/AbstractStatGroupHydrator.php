<?php
namespace StatBundle\Hydrator;

use ShortListBundle\Entity\ShortList;
use StatBundle\Entity\Participant;
use StatBundle\Entity\ParticipantList;
use StatBundle\Entity\PositionGroup;
use StatBundle\Entity\PositionGroupList;
use StatBundle\Entity\StatGroupList;
use StatBundle\Utils\PropValueTrait;

abstract class AbstractStatGroupHydrator {
    use PropValueTrait;

    protected $formation;

    public function __construct($formation)
    {
        $this->formation = $formation;
    }

    abstract protected function getData();

    public function hydrate(StatGroupList $statGroupList, $data)
    {
        $sportData = $this->getData();
        $statGroupList->setId($this->getPropValue($data, 'id'));

        $statGroupList->setPositionGroups($this->hydratePositionsList(new PositionGroupList(), $data->positions, $sportData));
        $statGroupList = $this->sortGroups($statGroupList);
        return $statGroupList;
    }

    private function sortGroups(StatGroupList $statGroupList) {
        foreach($statGroupList->getPositionGroups() as $positionGroup) {
            $participants = $positionGroup->getParticipants()->getArrayCopy();
            usort($participants, [$this, "groupSort"]);
            $positionGroup->setParticipants($participants);

        }
        return $statGroupList;
    }

    public  function groupSort($a, $b)
    {
        if($a->getScore() < $b->getScore()) {
            return 1;
        }
        return 0;
    }

    private function hydratePositionsList(PositionGroupList $groupList, $data, $sportData)
    {
        dump($sportData);
        $associatedGroups = [];
        foreach ($sportData->positions as $groupPos => $groupPosition) {
            $group = new PositionGroup();
            $group->setGroupName($this->getPropValue($groupPosition, 'group'));
            $group->setPositionDisplayName($this->getPropValue($groupPosition, 'positionDisplayName'));
            $group->setPositionName($this->getPropValue($groupPosition, 'positionName'));
            $group->setSubGroupName($this->getPropValue($groupPosition, 'subGroup'));
            $group->setParticipants(new ParticipantList());
            $groupList->addPos($group->getSubGroupName(), $group);
            $associatedGroups[$groupPos] = $group->getSubGroupName();

        }

        foreach($data as $pos => $item) {


            foreach($item->participants as $rawParticipant) {

                $participant = new Participant();
                $participant->setId($this->getPropValue($rawParticipant, 'id'));
                $participant->setScore($this->getPropValue($rawParticipant, 'score'));
                $groupName = $associatedGroups[$this->getPropValue($item, 'listPosition')];

                $searchedValue = $participant->getId();

                $neededObject = array_filter(
                    $groupList->offsetGet($groupName)->getParticipants()->getArrayCopy(),
                    function ($e) use (&$searchedValue) {
                        return $e->getId() == $searchedValue;
                    }
                );

                if(!empty($neededObject)) {
                    $existingParticipant = reset($neededObject);
                    $existingParticipantScore = $existingParticipant->getScore();
                    $participantScore = $participant->getScore();
                    $existingParticipant->setScore($existingParticipantScore + $participantScore);

                    $existingParticipantPos = key($neededObject);
                    $groupList->offsetGet($groupName)->getParticipants()->offsetSet($existingParticipantPos, $existingParticipant);
                } else {
                    $groupList->offsetGet($groupName)->getParticipants()->add($participant);
                }
            }
        }
        return $groupList;
    }


    public function hydrateParticipantFromShortList(StatGroupList $list, ShortList $shortList)
    {
        foreach ($list->getPositionGroups() as $position) {
            foreach ($position->getParticipants() as $participant) {

                $searchedValue = $participant->getId();
                $neededObject = array_values(array_filter(
                    $shortList->getParticipants()->getArrayCopy(),
                    function ($e) use (&$searchedValue) {
                        return $e->getId() == $searchedValue;
                    }
                ));
                $participant->setName($neededObject[0]->getLongName());
                $participant->setImage($neededObject[0]->getImage());

            }
        }
        return $list;
    }

    /**
     * @param StatGroupList $list
     * @param ShortList $shortList
     * @return StatGroupList
     */
    public function hydrateFormationFromShortList(StatGroupList $list, ShortList $shortList)
    {
        $list->setDefaultFormation($shortList->getFormation());
        return $list;
    }
}