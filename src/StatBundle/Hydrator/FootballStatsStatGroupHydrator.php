<?php

namespace StatBundle\Hydrator;

class FootballStatsStatGroupHydrator extends AbstractStatGroupHydrator
{

    protected function getData()
    {
        $json = <<<EOF
{
  "334": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 30
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 30
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 30
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 55
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 55
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 55
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 80
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 80
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 80
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 80
      }
    }
  },
  "343": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 30
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 30
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 30
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 55
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 55
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 55
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 55
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 80
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 80
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 80
      }
    }
  },
  "352": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "defender"
        ],
        "x": 25,
        "y": 30
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "defender"
        ],
        "x": 50,
        "y": 30
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "defender"
        ],
        "x": 75,
        "y": 30
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 11,
        "y": 60
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 30,
        "y": 53
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 50
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 70,
        "y": 53
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 89,
        "y": 60
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 32,
        "y": 80
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 66,
        "y": 80
      }
    }
  },
  "361": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "defender"
        ],
        "x": 25,
        "y": 30
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "defender"
        ],
        "x": 50,
        "y": 30
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "defender"
        ],
        "x": 75,
        "y": 30
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 7,
        "y": 61
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 23,
        "y": 56
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 41,
        "y": 54
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 59,
        "y": 54
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 77,
        "y": 56
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 93,
        "y": 61
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 80
      }
    }
  },
  "424": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 30
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 30
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 30
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 30
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 55
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 55
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 80
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 80
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 80
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 80
      }
    }
  },
  "433": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 30
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 30
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 30
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 30
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 55
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 55
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 55
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 17,
        "y": 80
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 80
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 83,
        "y": 80
      }
    }
  },
  "442": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 30
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 30
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 30
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 30
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 55
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 55
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 55
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 55
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 80
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 80
      }
    }
  },
  "451": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 30
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 30
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 30
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 30
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 11,
        "y": 60
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 30,
        "y": 53
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 50
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 70,
        "y": 53
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 89,
        "y": 60
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 80
      }
    }
  },
  "523": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 11,
        "y": 35
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 30,
        "y": 28
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 25
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 70,
        "y": 28
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 89,
        "y": 35
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 30,
        "y": 55
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 70,
        "y": 55
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 80
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 80
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 80
      }
    }
  },
  "532": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 11,
        "y": 35
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 30,
        "y": 28
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 25
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 70,
        "y": 28
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 89,
        "y": 35
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 55
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 55
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 55
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 30,
        "y": 80
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 70,
        "y": 80
      }
    }
  },
  "541": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 5
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 11,
        "y": 35
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 30,
        "y": 28
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 25
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 70,
        "y": 28
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 89,
        "y": 35
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 55
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 55
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 55
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 55
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 80
      }
    }
  },
  "3142": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 42
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 62
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 62
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 62
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 62
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 82
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 82
      }
    }
  },
  "3331": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 42
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 42
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 42
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 62
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 62
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 62
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 82
      }
    }
  },
  "3412": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 42
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 42
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 42
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 42
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 62
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 82
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 82
      }
    }
  },
  "3421": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 42
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 42
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 42
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 42
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 62
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 62
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 82
      }
    }
  },
  "3511": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 11,
        "y": 52
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 30,
        "y": 45
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 42
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 70,
        "y": 45
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 89,
        "y": 52
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 62
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 82
      }
    }
  },
  "4132": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 22
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 42
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 62
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 62
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 62
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 82
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 82
      }
    }
  },
  "4141": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 22
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 42
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 62
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 62
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 62
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 62
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 82
      }
    }
  },
  "4222": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 22
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 42
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 42
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 62
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 62
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 82
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 82
      }
    }
  },
  "4231": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 22
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 42
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 42
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 62
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 62
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 62
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 82
      }
    }
  },
  "4312": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 22
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 42
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 42
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 42
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 62
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 82
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 82
      }
    }
  },
  "4321": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 22
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 42
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 42
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 42
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 62
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 62
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 82
      }
    }
  },
  "4411": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 22
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 22
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 22
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 22
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 42
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 42
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 42
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 42
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 62
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 82
      }
    }
  },
  "31312": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 20
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 20
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 20
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 37
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 25,
        "y": 54
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 54
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 75,
        "y": 54
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 71
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 32,
        "y": 88
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 67,
        "y": 88
      }
    }
  },
  "41212": {
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "goalkeeper",
        "positionName": "goalkeeper",
        "positionDisplayName": "Goalkeeper",
        "filter": [
          "Goalkeeper",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 2
      },
      "2": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 12,
        "y": 20
      },
      "3": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 20
      },
      "4": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 20
      },
      "5": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "defender",
        "positionName": "defender",
        "positionDisplayName": "Defender",
        "filter": [
          "Defender",
          "Unknown",
          "Utility"
        ],
        "x": 87,
        "y": 20
      },
      "6": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 37
      },
      "7": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 54
      },
      "8": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 54
      },
      "9": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "midfielder",
        "positionName": "midfielder",
        "positionDisplayName": "Midfielder",
        "filter": [
          "Midfielder",
          "Attacking Midfielder",
          "Unknown",
          "Utility"
        ],
        "x": 50,
        "y": 71
      },
      "10": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 37,
        "y": 88
      },
      "11": {
        "id": "1",
        "label": false,
        "group": "all",
        "subGroup": "striker",
        "positionName": "striker",
        "positionDisplayName": "Striker",
        "filter": [
          "Striker",
          "Unknown",
          "Utility"
        ],
        "x": 62,
        "y": 88
      }
    }
  },
  "25": {
    "name": "squad23",
    "openPositions": "true",
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 3
      },
      "2": {
        "id": "2",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 3
      },
      "3": {
        "id": "3",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 3
      },
      "4": {
        "id": "4",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 3
      },
      "5": {
        "id": "5",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 20
      },
      "6": {
        "id": "6",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 20
      },
      "7": {
        "id": "7",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 20
      },
      "8": {
        "id": "8",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 20
      },
      "9": {
        "id": "9",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 37
      },
      "10": {
        "id": "10",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 37
      },
      "11": {
        "id": "11",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 37
      },
      "12": {
        "id": "12",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 37
      },
      "13": {
        "id": "13",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 54
      },
      "14": {
        "id": "14",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 54
      },
      "15": {
        "id": "15",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 54
      },
      "16": {
        "id": "16",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 54
      },
      "17": {
        "id": "17",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 71
      },
      "18": {
        "id": "18",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 71
      },
      "19": {
        "id": "19",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 71
      },
      "20": {
        "id": "20",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 71
      },
      "21": {
        "id": "21",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 25,
        "y": 88
      },
      "22": {
        "id": "22",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 50,
        "y": 88
      },
      "23": {
        "id": "23",
        "label": false,
        "group": "default",
        "subGroup": "squad23",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 75,
        "y": 88
      }
    }
  },
  "26": {
    "name": "squad24",
    "openPositions": "true",
    "positions": {
      "1": {
        "id": "1",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 3
      },
      "2": {
        "id": "2",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 3
      },
      "3": {
        "id": "3",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 3
      },
      "4": {
        "id": "4",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 3
      },
      "5": {
        "id": "5",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 20
      },
      "6": {
        "id": "6",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 20
      },
      "7": {
        "id": "7",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 20
      },
      "8": {
        "id": "8",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 20
      },
      "9": {
        "id": "9",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 37
      },
      "10": {
        "id": "10",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 37
      },
      "11": {
        "id": "11",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 37
      },
      "12": {
        "id": "12",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 37
      },
      "13": {
        "id": "13",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 54
      },
      "14": {
        "id": "14",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 54
      },
      "15": {
        "id": "15",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 54
      },
      "16": {
        "id": "16",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 54
      },
      "17": {
        "id": "17",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 71
      },
      "18": {
        "id": "18",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 71
      },
      "19": {
        "id": "19",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 71
      },
      "20": {
        "id": "20",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 71
      },
      "21": {
        "id": "21",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 12,
        "y": 88
      },
      "22": {
        "id": "22",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 37,
        "y": 88
      },
      "23": {
        "id": "23",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 63,
        "y": 88
      },
      "24": {
        "id": "24",
        "label": false,
        "group": "default",
        "subGroup": "squad24",
        "positionName": "all",
        "positionDisplayName": "Player",
        "positionVariations": [],
        "x": 88,
        "y": 88
      }
    }
  }
}


EOF;
        $formation = $this->formation;
        if ($formation == 'squad23') {
            $formation = 25;
        }
        if ($formation == 'squad24') {
            $formation = 26;
        }

        return json_decode($json)->{$formation};
    }
}