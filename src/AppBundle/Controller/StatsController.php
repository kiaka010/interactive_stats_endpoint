<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use ShortListBundle\Entity\ShortList;
use ShortListBundle\Hydrator\ShortListHydrator;
use ShortListBundle\Mapper\ShortListMapper;
use StatBundle\Entity\StatGroupList;
use StatBundle\Entity\StatList;
use StatBundle\Hydrator\StatGroupHydratorFactory;
use StatBundle\Hydrator\StatListHydrator;
use StatBundle\Mapper\StatMapper;
use StatBundle\Transformer\StatTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class StatsController extends Controller
{


    /**
     * @Route("/", name="index-page")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/home.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,

        ]);

    }

    /**
     * @Route("/", name="index-page-post")
     * @Method("POST")
     */
    public function articleProcessAction(Request $request)
    {
        $matches = null;
        if ($url = $request->request->get('articleUrl')) {
            $html = file_get_contents($url);
            $res = preg_match_all("/url=\"https:\/\/api.condatis.sky\/interactive\/shortlist\/[a-f0-9]{32}/i", $html,
                $matches);

        }
        dump($matches);
        if (!is_array($matches) && empty($matches)) {
            die("Matches empty");
        }
        str_replace('url="http://api.condatis.sky/interactive/shortlist/', '', $matches[0][0]);
        if (count($matches[0]) == 1) {
            return $this->redirectToRoute('statpage', array(
                'statId' => str_replace('url="https://api.condatis.sky/interactive/shortlist/', '', $matches[0][0]),
                'limit' => 1
            ));
        } else {
            $statIds = [];
            foreach ($matches[0] as $url)
            {
                $statIds[] = str_replace('url="https://api.condatis.sky/interactive/shortlist/', '', $url);
            }
            return $this->redirectToRoute('multi-statpage', ['urls' => implode(':', $statIds)]);
        }

    }

    /**
     * @param Request $request
     * @Route("/multiple-found", name="multi-statpage")
     */
    public function selectPage(Request $request)
    {
        return $this->render('default/multi.stat.html.twig', [
            'statIds' => explode(':', $request->get('urls')),

        ]);
    }
//    /**
//     * @Route("/stats/{limit}", name="stat-page-raw-post", defaults={"limit" = null})
//     * @Method("POST")
//     */
//    public function rawPage(Request $request, $limit)
//    {
//        $shortListRawData = $request->request->get('rawStatData');
//        //Short List
//
//        $formattedData = StatTransformer::transformRaw($shortListRawData);
//        $shortListHydrator = new ShortListHydrator();
//        $shortListHydrator->hydrate($shortlist = new ShortList(), $formattedData);
//
//        // Stat List
//        $statMapper = new StatMapper($this->container, $this->get('logger'));
//        $statRawData = $statMapper->getById($statId);
//        $formattedData = StatTransformer::transformRaw($statRawData);
//
//        $statListHydrator = new StatListHydrator($limit);
//        $statListHydrator->hydrate($statList = new StatList(), $formattedData);
//
//
//        // Stat Group List
//        $statGroupHydrator = (new StatGroupHydratorFactory($shortlist->getFormation()))->create($shortlist->getType());
//        $statGroupHydrator->hydrate($statGroupList = new StatGroupList(), $formattedData);
//
//        //hydrate stat list participants
//        $statListHydrator->hydrateParticipantFromShortList($statList, $shortlist);
//        $statGroupHydrator->hydrateParticipantFromShortList($statGroupList, $shortlist);
//
//        //hydrate stat list default formations
//        $statListHydrator->hydrateFormationFromShortList($statList, $shortlist);
//        $statGroupHydrator->hydrateFormationFromShortList($statGroupList, $shortlist);
//
//        // replace this example code with whatever you need
//        return $this->render('default/stats.html.twig', [
//            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
//            'baseUrl' => $this->generateUrl('statpage', array('statId' => $statId)),
//            'stats' => $statList,
//            'groupStats' => $statGroupList,
//            'limit' => $limit
//        ]);
//    }

    /**
     * @Route("/stats/{statId}/{limit}", name="statpage", defaults={"limit" = null})
     */
    public function statAction(Request $request, $statId, $limit)
    {

        //Short List
        $shortListMapper = new ShortListMapper($this->container, $this->get('logger'));
        $shortListRawData = $shortListMapper->getById($statId);
        $formattedData = StatTransformer::transformRaw($shortListRawData);
        $shortListHydrator = new ShortListHydrator();
        $shortListHydrator->hydrate($shortlist = new ShortList(), $formattedData);

        // Stat List
        $statMapper = new StatMapper($this->container, $this->get('logger'));
        $statRawData = $statMapper->getById($statId);
        $formattedData = StatTransformer::transformRaw($statRawData);

        $statListHydrator = new StatListHydrator($limit);
        $statListHydrator->hydrate($statList = new StatList(), $formattedData);


        // Stat Group List
        $statGroupHydrator = (new StatGroupHydratorFactory($shortlist->getFormation()))->create($shortlist->getType());
        $statGroupHydrator->hydrate($statGroupList = new StatGroupList(), $formattedData);

        //hydrate stat list participants
        $statListHydrator->hydrateParticipantFromShortList($statList, $shortlist);
        $statGroupHydrator->hydrateParticipantFromShortList($statGroupList, $shortlist);

        //hydrate stat list default formations
        $statListHydrator->hydrateFormationFromShortList($statList, $shortlist);
        $statGroupHydrator->hydrateFormationFromShortList($statGroupList, $shortlist);
//        dump($statList);
        // replace this example code with whatever you need
        return $this->render('default/stats.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..') . DIRECTORY_SEPARATOR,
            'baseUrl' => $this->generateUrl('statpage', array('statId' => $statId)),
            'stats' => $statList,
            'groupStats' => $statGroupList,
            'limit' => $limit
        ]);
    }
}
